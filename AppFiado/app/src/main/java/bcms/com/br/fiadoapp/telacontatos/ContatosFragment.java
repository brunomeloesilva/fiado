package bcms.com.br.fiadoapp.telacontatos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import bcms.com.br.fiadoapp.MainActivity;
import bcms.com.br.fiadoapp.R;
import bcms.com.br.fiadoapp.conta.MeuBaseExpandableListAdapter;
import bcms.com.br.fiadoapp.telafiado.TelaFiado;

/**
 * Created by brunosilva on 04/11/15.
 */
public class ContatosFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contatos_fragment, container, false);
        final ListView listViewClientes = (ListView) view.findViewById(R.id.IdListViewContatos);
        listViewClientes.setAdapter(MainActivity.contatosAdapter);

        listViewClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, String> map = (HashMap<String, String>) listViewClientes.getAdapter().getItem(position);
                final String idPhone = map.get("keyIdTelefonico");
                final String nome = map.get("keyNome");

                //Se o cliente deu pino na ultima compra, alerta o usuario
                if (MainActivity.gerenciadorBD.getStatusCliente(idPhone).equals("P")) {

                    final int pkCliente = MainActivity.gerenciadorBD.getPKTabela("Cliente", "IDtelefone = "+idPhone, null);

                    AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(getContext());
                    construtorAlerta.setTitle("ALERTA DE PINO");
                    construtorAlerta.setMessage("Este cliente já te deu pino, para desbloquea-lo , você pode reabrir a sua conta" +
                            " ou pode receber o valor do pino agora mesmo.");
                    construtorAlerta.setCancelable(true);
                    construtorAlerta.setPositiveButton("Reabrir", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MeuBaseExpandableListAdapter.reaberturaConta(getActivity(), ""+pkCliente, idPhone);
                        }
                    });
                    construtorAlerta.setNegativeButton("Receber valor do pino", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MeuBaseExpandableListAdapter.pagamentoComPino(getActivity(), idPhone, ""+pkCliente, nome);
                        }
                    });
                    AlertDialog alerta = construtorAlerta.create();
                    alerta.show();
                //Caso ele esteja com o status de cliente normal(nao tenha dado pino)
                // abre a tela de pedidos normalmente para realizar um fiado
                } else {
                    Intent i = new Intent(view.getContext(), TelaFiado.class);
                    i.putExtra("keyIdTelefonico", idPhone);
                    i.putExtra("keyNome", nome);
                    startActivity(i);
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Para atualizar o texto em vermelho para os clientes em status pino
        MainActivity.contatosAdapter.notifyDataSetChanged();
    }
}
