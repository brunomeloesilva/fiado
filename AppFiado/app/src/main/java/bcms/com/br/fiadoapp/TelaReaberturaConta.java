package bcms.com.br.fiadoapp;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by brunosilva on 08/11/15.
 */
public class TelaReaberturaConta extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finish();
    }
}
