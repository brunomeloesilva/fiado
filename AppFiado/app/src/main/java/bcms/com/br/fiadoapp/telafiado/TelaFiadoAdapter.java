package bcms.com.br.fiadoapp.telafiado;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import bcms.com.br.fiadoapp.R;

import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.getFloatNumberInstance;
import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.getStringNumberInstance;

/**
 * Created by brunosilva on 05/11/15.
 */
public class TelaFiadoAdapter extends BaseAdapter {

    private Context contexto;
    private List<Map<String, String>> listMap;
    private TreeSet<Integer> posicoesExclusao;

    public TelaFiadoAdapter(Context contexto, List<Map<String, String>> listMap) {
        this.contexto = contexto;
        this.listMap = listMap;
        this.posicoesExclusao = new TreeSet<>();
    }

    @Override
    public int getCount() {
        return listMap.size();
    }

    @Override
    public Object getItem(int position) {
        return listMap.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater infladorViewXML = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infladorViewXML.inflate(R.layout.tela_fiado_adapter_item, null);

        ((CheckBox) view.findViewById(R.id.IdCheckBoxItem)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {//ao marcar, se nao existir insere
                    if (!posicoesExclusao.contains(new Integer(position))) {
                        posicoesExclusao.add(new Integer(position));
                    }
                } else {//ao desmarcar, se existia, exclui
                    if (posicoesExclusao.contains(new Integer(position))) {
                        posicoesExclusao.remove(new Integer(position));
                    }
                }
            }
        });

        String itemPrecoUnit = String.format("%,.2f", new Float(listMap.get(position).get("keyPrecoUnitario")));
        String itemPrecoTotal = String.format("%,.2f", new Float(listMap.get(position).get("keyPrecoTotalItem")));
        //String itemQuantidadeString = String.format("%,.2f", new Float(listMap.get(position).get("keyQuantidade")));

        //Desta forma quando a quantidade nao tem casas decimais fica so a parte inteira.
        float itemQuantdadeFloat = getFloatNumberInstance(listMap.get(position).get("keyQuantidade"), null);
        String itemQuantidadeString = getStringNumberInstance(itemQuantdadeFloat, null);

        ((TextView) view.findViewById(R.id.IdTextViewProduto)).setText(listMap.get(position).get("keyDescricao"));
        ((TextView) view.findViewById(R.id.IdTextViewQuantidade)).setText(itemQuantidadeString);
        ((TextView) view.findViewById(R.id.IdTextViewPrecoUnitario)).setText(itemPrecoUnit);
        ((TextView) view.findViewById(R.id.IdTextViewTotalItem)).setText(itemPrecoTotal);
        return view;
    }

    public TreeSet<Integer> getPosicoesExclusao() {
        return posicoesExclusao;
    }
    public void limpaPosicoesExclusao() {
        posicoesExclusao = new TreeSet<>();
    }
}
