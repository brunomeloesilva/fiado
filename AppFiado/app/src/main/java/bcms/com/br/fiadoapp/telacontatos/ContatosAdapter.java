package bcms.com.br.fiadoapp.telacontatos;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bcms.com.br.fiadoapp.MainActivity;
import bcms.com.br.fiadoapp.R;

/**
 * Created by brunosilva on 04/11/15.
 */
public class ContatosAdapter extends BaseAdapter {

    private Context contexto;
    private List<Map<String, String>> listMap;
    //Sera feito uma copia de backup do listMap, pois irei trabalhar com filtro de busca e isso sera
    //necessario para fazer o restore da lista original
    private List<Map<String, String>> listMapBackup;
    private CharSequence textoBuscadoAnterior="";

    public ContatosAdapter(Context contexto) {
        this.contexto = contexto;
        preencherListMap();
    }

    private void preencherListMap(){
        listMap = new ArrayList<>();
        listMapBackup = new ArrayList<>();

        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        //Colunas:
        String nome = ContactsContract.Contacts.DISPLAY_NAME;
        String pk = ContactsContract.Contacts._ID;
        String colunas[] = new String[]{pk, nome};
        //Restricoes:
        //Retorna 1 se possui pelo menos um numero de telefone ou 0 se nenhum.
        String hasTelefone = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        String restricao = colunas[1]+" is not null and "+hasTelefone+"=1";
        //Ordenacao:
        String ordenacao = colunas[1];

        Cursor cursor = contexto.getContentResolver().query(uri,colunas,restricao,null,ordenacao);

        Map<String, String> map;
        while(cursor.moveToNext()) {
            map = new HashMap<>();
                map.put("keyNome", cursor.getString(1));
                map.put("keyIdTelefonico", cursor.getString(0));
                listMap.add(map);
                listMapBackup.add(map);
        }
        cursor.close();
    }

    public void atualizarAdapter() {
        this.preencherListMap();
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listMap.size();
    }
    @Override
    public Object getItem(int position) {
        return listMap.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflarLayout = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflarLayout.inflate(R.layout.contatos_adapter_item, null);
        TextView txtNome = (TextView) view.findViewById(R.id.IdTextViewNome);
        txtNome.setText(listMap.get(position).get("keyNome"));
        //Para pintar o nome do cliente de vermelho, caso o status dele esteja como pino
        if(MainActivity.gerenciadorBD.getStatusCliente(listMap.get(position).get("keyIdTelefonico")).equals("P"))
            txtNome.setTextColor(Color.RED);
        return view;
    }

    //Para o filtro de busca a ser usado pela toolbar com a lupa
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence textoBuscado) {
                // TODO Auto-generated method stub
                FilterResults Result = new FilterResults();
                //Quando o campo de bucar ficar vazio, retorna o listMap original.
                if(textoBuscado.length() == 0 ){
                    Result.values = listMapBackup;
                    Result.count = listMapBackup.size();
                    return Result;
                }

                //Inicio do processo de comparacao e busca, a busca é baseada no listMap
                //Restarta o listMap para garantir a busca na volta, ou seja, quando os
                // caracteres vao sendo apagados no campo de busca
                    Result.values = listMapBackup;
                    Result.count = listMapBackup.size();
                    listMap = (List<Map<String, String>>) ((FilterResults) Result).values;

                    List<Map<String, String>> nomesFiltrados = new ArrayList<>();
                //Trata o texto botando tudo pra minusculo e retirando os acentos para melhor comparacao
                    String textoBuscadoTratado = textoBuscado.toString().toLowerCase();
                    textoBuscadoTratado = Normalizer.normalize(textoBuscadoTratado, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

                    String textoDaLista;
                    String textoDaListaTratado;
                    Map<String, String> map;
                    for(int i = 0; i < listMap.size(); i++){
                        textoDaLista = listMap.get(i).get("keyNome");
                        //Trata o texto botando tudo pra minusculo e retirando os acentos para melhor comparacao
                        textoDaListaTratado = textoDaLista.toLowerCase();
                        textoDaListaTratado = Normalizer.normalize(textoDaListaTratado, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                        //faz a comparacao
                        if(textoDaListaTratado.contains(textoBuscadoTratado)){
                            map = new HashMap<>();
                            map.put("keyNome", textoDaLista);
                            nomesFiltrados.add(map);
                        }
                    }
                    Result.values = nomesFiltrados;
                    Result.count = nomesFiltrados.size();
                    return Result;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                // TODO Auto-generated method stub
                listMap = (List<Map<String, String>>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}