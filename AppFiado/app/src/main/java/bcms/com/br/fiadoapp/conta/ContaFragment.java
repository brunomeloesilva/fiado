package bcms.com.br.fiadoapp.conta;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.List;
import java.util.Map;

import bcms.com.br.fiadoapp.MainActivity;
import bcms.com.br.fiadoapp.R;

/**
 * Created by brunosilva on 04/11/15.
 */
public class ContaFragment extends Fragment {

    //É uma Map que usa primitivo na key, ao inves de Object, portanto Mas eficiente.
    private SparseArray<PaiFilhos> map;
    public MeuBaseExpandableListAdapter adapter;

    private void alimentarSparseArray() {
        map.clear();//deve mudar a logica para melhor desempenho
        List<Map<String, String>> listMapsContas = MainActivity.gerenciadorBD.getListMapContasPai();
        PaiFilhos mestreDetalhe;
        for(int i=0; i < listMapsContas.size(); i++) {
            mestreDetalhe = new PaiFilhos(listMapsContas.get(i).get("keyPK")
                    , listMapsContas.get(i).get("keyIdTelefonico")
                    , listMapsContas.get(i).get("keyNome")
                    , listMapsContas.get(i).get("keyTotalConta")
                    , MainActivity.gerenciadorBD.getListMapContasFilhos(listMapsContas.get(i).get("keyPK")));
            map.append(i, mestreDetalhe);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.conta_fragment, container, false);
        map = new SparseArray<>();
        alimentarSparseArray();
        ExpandableListView expandableListView = (ExpandableListView) view.findViewById(R.id.IdExpandableListView);
        adapter = new MeuBaseExpandableListAdapter(getActivity(), map);
        expandableListView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Para atualizar os itens da tela: Rescrever para aumentar desenpenho.
        alimentarSparseArray();
        adapter.notifyDataSetChanged();
    }
}
