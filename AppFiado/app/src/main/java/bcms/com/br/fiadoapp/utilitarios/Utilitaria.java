package bcms.com.br.fiadoapp.utilitarios;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brunosilva on 24/08/15.
 * Os valores tem que entrar e sair no mesmo padrao de regionalizacao.
 */
public final class Utilitaria {

    private static final String IDIOMA = "pt";
    private static final String PAIS = "BR";

    public static float getFloatCurrencyInstance(String NumeroCurrencyInstance, Locale regionalizacao) {
        regionalizacao = regionalizacao != null ? regionalizacao : new Locale(IDIOMA, PAIS);
        NumberFormat f = NumberFormat.getCurrencyInstance(regionalizacao);
        float numero = 0;
        try {
            numero = f.parse(NumeroCurrencyInstance).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numero;
    }

    public static String getStringCurrencyInstance(float numero, Locale regionalizacao) {
        regionalizacao = regionalizacao != null ? regionalizacao : new Locale(IDIOMA, PAIS);
        NumberFormat f = NumberFormat.getCurrencyInstance(regionalizacao);
        return f.format(numero);
    }

    //Usado para quantidades
    public static String getStringNumberInstance(float numero, Locale regionalizacao) {
        regionalizacao = regionalizacao != null ? regionalizacao : new Locale(IDIOMA, PAIS);
        NumberFormat f = NumberFormat.getNumberInstance(regionalizacao);
        f.setMaximumFractionDigits(2);
        return f.format(numero);
    }

    //Usado para valores financeiros
    public static String getStringNumberInstance$(float numero, Locale regionalizacao) {
        regionalizacao = regionalizacao != null ? regionalizacao : new Locale(IDIOMA, PAIS);
        NumberFormat f = NumberFormat.getNumberInstance(regionalizacao);
        f.setMaximumFractionDigits(2);
        f.setMinimumFractionDigits(2);
        return f.format(numero);
    }

    public static float getFloatNumberInstance(String NumeroNumberInstance, Locale regionalizacao) {
        regionalizacao = regionalizacao != null ? regionalizacao : new Locale(IDIOMA, PAIS);
        NumberFormat f = NumberFormat.getNumberInstance(regionalizacao);
        float numero = new Float(NumeroNumberInstance);
        try {
            numero = f.parse(NumeroNumberInstance).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numero;
    }

    public static String dataAtual(Locale regionalizacao){
        regionalizacao = regionalizacao != null ? regionalizacao : new Locale(IDIOMA, PAIS);
        DateFormat formatoData = DateFormat.getDateInstance(DateFormat.MEDIUM, regionalizacao);
        return formatoData.format(Calendar.getInstance().getTime());
    }

    public static String getDataFormatada(String data) {
        //String para Date
        SimpleDateFormat formatadorDataEntrada = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dataAux = null;
        try {
            dataAux = formatadorDataEntrada.parse(data);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //Date para  String
        Locale local = new Locale(IDIOMA,PAIS);
        DateFormat formatadorDataSaida = DateFormat.getDateInstance(DateFormat.FULL, local);
        String dataFormatada = formatadorDataSaida.format(dataAux);
        return dataFormatada;
    }

    public static BigDecimal roundDouble(double valor) {
        BigDecimal bigDecimal = new BigDecimal(valor);
        //arredonda se > 5
        bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        return bigDecimal;
    }
}

