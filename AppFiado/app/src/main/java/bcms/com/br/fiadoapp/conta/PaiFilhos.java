package bcms.com.br.fiadoapp.conta;

import java.util.List;
import java.util.Map;

/**
 * Created by brunosilva on 06/11/15.
 */
//Servirá para alimentar o Group e Child do BaseExpandableListAdapter
public class PaiFilhos {

    //Para o Group
    public String pkCliente;
    public String pkIdTelefonico;
    public String nomeCliente;
    public String totalConta;
    //Para o Child
    public List<Map<String, String>> filhos;


    public PaiFilhos(String pkCliente
            , String pkIdTelefonico
            , String nomeCliente
            , String totalConta
            , List<Map<String, String>> filhos)
    {
        this.pkCliente = pkCliente;
        this.pkIdTelefonico = pkIdTelefonico;
        this.nomeCliente = nomeCliente;
        this.totalConta = totalConta;
        this.filhos = filhos;
    }
}
