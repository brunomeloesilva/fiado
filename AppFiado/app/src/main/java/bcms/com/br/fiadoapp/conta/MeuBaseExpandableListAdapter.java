package bcms.com.br.fiadoapp.conta;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

import bcms.com.br.fiadoapp.MainActivity;
import bcms.com.br.fiadoapp.R;
import bcms.com.br.fiadoapp.TelaReaberturaConta;
import bcms.com.br.fiadoapp.TelaRecebimento;
import bcms.com.br.fiadoapp.telafiado.TelaFiado;
import bcms.com.br.fiadoapp.utilitarios.Utilitaria;

import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.getStringNumberInstance$;

/**
 * Created by brunosilva on 06/11/15.
 */
public class MeuBaseExpandableListAdapter extends BaseExpandableListAdapter {

    //É um Map que usa primitivo na key, ao inves de Object.
    private final SparseArray<PaiFilhos> mapPaiFilhos;
    private LayoutInflater inflater;
    private Activity contexto;

    public MeuBaseExpandableListAdapter(Activity contexto, SparseArray<PaiFilhos> mapPaiFilhos) {
        this.contexto = contexto;
        this.mapPaiFilhos = mapPaiFilhos;
        this.inflater = (LayoutInflater) this.contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getGroupCount() {
        return mapPaiFilhos.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mapPaiFilhos.get(groupPosition).filhos.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mapPaiFilhos.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mapPaiFilhos.get(groupPosition).filhos.get(childPosition);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.meu_base_expandable_list_adapter_mestre, null);
        }

        final PaiFilhos mestre = (PaiFilhos) this.getGroup(groupPosition);
        TextView txtNome = (TextView) convertView.findViewById(R.id.IdTextViewCliente);
        txtNome.setText("" + mestre.nomeCliente);
        ImageView btnFiado = (ImageView) convertView.findViewById(R.id.IdImageViewFiado);
        ImageView btnPagar = (ImageView) convertView.findViewById(R.id.IdImageViewPagar);
        final TextView txtDivida = (TextView) convertView.findViewById(R.id.IdTextViewDivida);
        //Caso a classe que chamou foi a de xHistorico
        if(parent.getContext().getClass().getSimpleName().equals("TelaHistorico")) {
            txtDivida.setText("");
            btnFiado.setImageResource(R.drawable.nada);
            btnPagar.setImageResource(R.drawable.nada);
            btnFiado.setEnabled(false);
            btnPagar.setEnabled(false);
            //Para pintar os pinos diferenciadamente
            //for (Map<String, String> detalhe : mestre.filhos) {
                if(MainActivity.gerenciadorBD.getStatusCliente(mestre.pkIdTelefonico).equals("P")) {
                    btnFiado.setEnabled(true);
                    btnPagar.setEnabled(true);
                    convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorVermelhoPino));
                    btnPagar.setImageResource(R.drawable.cifrao);
                    btnPagar.setOnClickListener(new ImageButton.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pagamentoComPino(contexto, mestre.pkIdTelefonico, mestre.pkCliente, mestre.nomeCliente);
                        }
                    });
                    btnFiado.setImageResource(R.drawable.up);
                    btnFiado.setOnClickListener(new ImageButton.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reaberturaConta(contexto, mestre.pkCliente, mestre.pkIdTelefonico);
                        }
                    });
                }else{
                    convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorVerdePagamento));
                }
            //}

        } else {
            txtDivida.setText(mestre.totalConta);

            btnFiado.setOnClickListener(new ImageButton.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(contexto, TelaFiado.class);
                    i.putExtra("keyNome", mestre.nomeCliente);
                    i.putExtra("keyIdTelefonico", mestre.pkIdTelefonico);
                    contexto.startActivity(i);
                }
            });
            btnPagar.setOnClickListener(new ImageButton.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(contexto, TelaRecebimento.class);
                    i.putExtra("keyPK", mestre.pkCliente);
                    i.putExtra("keyNome", mestre.nomeCliente);
                    i.putExtra("keyDivida", mestre.totalConta);
                    i.putExtra("keyIdTelefonico", mestre.pkIdTelefonico);
                    contexto.startActivity(i);
                }
            });
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.meu_base_expandable_list_adapter_detalhe, null);
        }
        //boolean isTelaHistorico = parent.getContext().getClass().getSimpleName().equals("TelaHistorico");
        final Map<String, String> detalhe = (Map<String, String>) getChild(groupPosition, childPosition);
        String descricaoItem = detalhe.get("keyDescricao");
       ((TextView) convertView.findViewById(R.id.IdTextViewProduto)).setText(descricaoItem);
        ((TextView) convertView.findViewById(R.id.IdTextViewPrecoTotal)).setText(detalhe.get("keyPrecoTotal"));

        //mostra a data do item
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = Utilitaria.getDataFormatada(detalhe.get("keyData"));
                Toast.makeText(contexto, data, Toast.LENGTH_SHORT).show();
            }
        });
        if(descricaoItem.equals("Recebido")) {
            /*/pra zebrar as cores do historico
            if(isTelaHistorico) {
                    if(new Integer(detalhe.get("keyFKFiado")) % 2 == 0) {
                        convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorFundoA));
                    }else {
                        convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorFundoB));
                    }
            } else*/
                convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorVerdePagamento));

            ((TextView) convertView.findViewById(R.id.IdTextViewX)).setText("");
            ((TextView) convertView.findViewById(R.id.IdTextViewQuantidade)).setText("");
            ((TextView) convertView.findViewById(R.id.IdTextViewPrecoUnitario)).setText("");
        }
        else if(descricaoItem.equals("Pino")) {//Porque tou usando este para a tela de historico.
            convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorVermelhoPino));
            ((TextView) convertView.findViewById(R.id.IdTextViewX)).setText("");
            ((TextView) convertView.findViewById(R.id.IdTextViewQuantidade)).setText("");
            ((TextView) convertView.findViewById(R.id.IdTextViewPrecoUnitario)).setText("");
        }
        else {
            /*/pra zebrar as cores do historico
            if(isTelaHistorico) {
                    if(new Integer(detalhe.get("keyFKFiado")) % 2 == 0) {
                        convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorFundoA));
                    }else {
                        convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorFundoB));
                    }
            }else*/
                convertView.setBackgroundColor(ContextCompat.getColor(contexto, R.color.CorFundo));

            ((TextView) convertView.findViewById(R.id.IdTextViewX)).setText("x");
            ((TextView) convertView.findViewById(R.id.IdTextViewQuantidade)).setText(detalhe.get("keyQuantidade"));
            ((TextView) convertView.findViewById(R.id.IdTextViewPrecoUnitario)).setText(detalhe.get("keyPrecoUnit"));

        }
        return convertView;
    }

    @Override//Nao Sobrescrito
    public long getGroupId(int groupPosition) {
        return 0;
    }
    @Override//Nao Sobrescrito
    public long getChildId(int groupPosition, int childPosition) { return 0; }
    @Override//Nao Sobrescrito
    public boolean hasStableIds() {
        return false;
    }
    @Override//Nao Sobrescrito
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    //Encapsulamento de processos, pois será usado fora daqui
    public static void pagamentoComPino(Context contexto, String pkIdTelefonico, String pKCliente, String nomeCliente){
        //Pegar o valor do pino
        float divida = MainActivity.gerenciadorBD.getValorPino(pkIdTelefonico);
        divida = -divida;
        String txtDivida = getStringNumberInstance$(divida, null);

        Intent i = new Intent(contexto, TelaRecebimento.class);
        i.putExtra("keyPK", pKCliente);
        i.putExtra("keyNome", nomeCliente);
        i.putExtra("keyDivida", txtDivida);
        i.putExtra("keyIdTelefonico", pkIdTelefonico);
        i.putExtra("keySqPino", MainActivity.gerenciadorBD.getSqPino(pkIdTelefonico));
        contexto.startActivity(i);
    }

    public static void reaberturaConta(final Activity contexto, final String pkCliente, final String pkIdTelefonico){
        AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(contexto);
        construtorAlerta.setTitle("REABERTURA DE CONTA");
        construtorAlerta.setMessage("Você deseja reabrir esta conta ?");
        construtorAlerta.setCancelable(false);
        construtorAlerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Restaura a conta do cliente
                //Deletar registro do pino
                long pkFiado = MainActivity.gerenciadorBD.getPKTabela("Fiado", "FKCliente = ? AND Status = 'P'", new String[]{"" + pkCliente});
                int sqPino = MainActivity.gerenciadorBD.getSqPino(pkIdTelefonico);
                MainActivity.gerenciadorBD.deletaRegistros("ItensFiado"
                        , "FKFiado = ? And Sq = ?", new String[]{"" + pkFiado, "" + sqPino});
                //Trocar o status do Fiado pra A
                ContentValues parColunaValor = new ContentValues();
                parColunaValor.put("Status", "A");
                MainActivity.gerenciadorBD.atualizarRegistros("Fiado", parColunaValor, "PK = " + pkFiado, null);
                //Trocar o status do cliente pra A
                MainActivity.gerenciadorBD.atualizarRegistros("Cliente", parColunaValor, "PK = " + pkCliente, null);
                if (contexto.getClass().getSimpleName().equals("TelaHistorico")) contexto.finish();
                else {
                    MainActivity.contatosAdapter.notifyDataSetChanged();
                    //Criei esta activity apenas para forçar a chamada do metodo onResume
                    //da tela de contas, assim atualizará os dados da tela !
                    contexto.startActivity(new Intent(contexto, TelaReaberturaConta.class));
                }

            }
        });
        construtorAlerta.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        AlertDialog alerta = construtorAlerta.create();
        alerta.show();
    }
}
