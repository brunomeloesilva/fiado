package bcms.com.br.fiadoapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import bcms.com.br.fiadoapp.utilitarios.EnviarSMS;
import bcms.com.br.fiadoapp.utilitarios.Utilitaria;

/**
 * Created by brunosilva on 22/07/15.
 */
public class TelaRecebimento extends Activity {

    private EditText edtConta;
    private EditText edtRecebido;
    private EditText edtTroco;
    private String recebido;
    private boolean virgulaUsada;
    private Locale regionalizacao;

    private double $recebido;
    private double $conta;
    private double $troco;
    private int sqPino;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tela_recebimento);

        Bundle pacote = getIntent().getExtras();

        ((TextView) findViewById(R.id.txt_tp_nome)).setText(pacote.getString("keyNome"));
        edtConta = (EditText) findViewById(R.id.edt_tp_conta);
        edtConta.setText(pacote.getString("keyDivida"));
        edtRecebido = (EditText) findViewById(R.id.edt_tp_recebido);
        edtTroco = (EditText) findViewById(R.id.edt_tp_troco);


        ((Button) findViewById(R.id.btn_tp_0)).setOnClickListener(new CliqueTeclado('0'));
        ((Button) findViewById(R.id.btn_tp_1)).setOnClickListener(new CliqueTeclado('1'));
        ((Button) findViewById(R.id.btn_tp_2)).setOnClickListener(new CliqueTeclado('2'));
        ((Button) findViewById(R.id.btn_tp_3)).setOnClickListener(new CliqueTeclado('3'));
        ((Button) findViewById(R.id.btn_tp_4)).setOnClickListener(new CliqueTeclado('4'));
        ((Button) findViewById(R.id.btn_tp_5)).setOnClickListener(new CliqueTeclado('5'));
        ((Button) findViewById(R.id.btn_tp_6)).setOnClickListener(new CliqueTeclado('6'));
        ((Button) findViewById(R.id.btn_tp_7)).setOnClickListener(new CliqueTeclado('7'));
        ((Button) findViewById(R.id.btn_tp_8)).setOnClickListener(new CliqueTeclado('8'));
        ((Button) findViewById(R.id.btn_tp_9)).setOnClickListener(new CliqueTeclado('9'));
        ((Button) findViewById(R.id.btn_tp_virgula)).setOnClickListener(new CliqueTeclado(','));
        ((ImageButton) findViewById(R.id.btn_tp_back)).setOnClickListener(new CliqueTeclado('B'));
        ((Button) findViewById(R.id.btn_tp_cancel)).setOnClickListener(new CliqueTeclado('C'));
        ((Button) findViewById(R.id.TRbtnReceber)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if($recebido!=0) {
                    //se for receber uma conta que ja possui o registro de pino, antes de fazer o
                    //recebimento normal, deve-se excluir o registro do pino
                    if(sqPino>0) {
                        //Deletar registro do pino
                        String pkCliente = getIntent().getStringExtra("keyPK");
                        long pkFiado = MainActivity.gerenciadorBD.getPKTabela("Fiado", "FKCliente = ? AND Status = 'P'", new String[]{"" + pkCliente});
                        MainActivity.gerenciadorBD.deletaRegistros("ItensFiado"
                               , "FKFiado = ? And Sq = ?" , new String[]{""+pkFiado, ""+sqPino});
                        //Trocar o status do Fiado pra A
                        ContentValues parColunaValor = new ContentValues();
                        parColunaValor.put("Status", "A");
                        MainActivity.gerenciadorBD.atualizarRegistros("Fiado", parColunaValor, "PK = " + pkFiado, null);
                        //Trocar o status do cliente pra A
                        MainActivity.gerenciadorBD.atualizarRegistros("Cliente", parColunaValor, "PK = " + pkCliente, null);
                    }
                    fazRecebimento("Recebido", ($recebido-$troco));
                }else{
                    Toast.makeText(getBaseContext(), "Digite o valor recebido !", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button btnPino = (Button) findViewById(R.id.TRbtnPino);
        btnPino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(TelaRecebimento.this);
                construtorAlerta.setTitle("CONFIRMAÇÃO DE PINO");
                construtorAlerta.setMessage("Você está indicando que esta é uma dívida perdida, que " +
                        "você jamais receberá, portanto você deseja retira-la da lista. Confirma o pino ?");
                construtorAlerta.setCancelable(false);
                construtorAlerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fazRecebimento("Pino", $conta);

                    }
                });
                construtorAlerta.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog alerta = construtorAlerta.create();
                alerta.show();
            }
        });

        //Caso seja um pagamento de pino do historico
        sqPino = pacote.getInt("keySqPino", 0);
        if(sqPino>0) {
            btnPino.setEnabled(false);
            btnPino.setTextColor(Color.GRAY);
        }


        recebido = "0";
        regionalizacao = new Locale("pt","BR");
        $conta = getDouble(edtConta.getText().toString());
    }

    private class CliqueTeclado implements View.OnClickListener {

        private Character tecla;

        public CliqueTeclado(char tecla) {
            this.tecla = new Character(tecla);
        }

        @Override
        public void onClick(View v) {
            if(Character.isDigit(tecla.charValue())) {
                if(virgulaUsada && (recebido.substring(recebido.indexOf(","), recebido.length()).length()) == 3)
                    recebido = recebido.substring(0, recebido.length()-1) + tecla;
                else
                    recebido = recebido + tecla;
            }else if(tecla.equals(',') && !virgulaUsada) {
                recebido = recebido + tecla;
                virgulaUsada = true;
            }else if(tecla.equals('B')){
                if(recebido.endsWith(",")) virgulaUsada = false;
                recebido = recebido.substring(0, (recebido.length() - 1 <= 0) ? 0 : recebido.length() - 1);
                recebido = recebido.isEmpty() ? "0" : recebido;
                //Toast.makeText(getBaseContext(),recebido,Toast.LENGTH_LONG).show();
            }else if(tecla.equals('C')) {
                finish();
            }
            $recebido = getDouble(recebido.length() == 0 ? "0" : recebido);
            $troco = $recebido - $conta < 0 ? 0 : $recebido - $conta;
            if(recebido.indexOf(",")!=-1)
                edtRecebido.setText(getStringDouble($recebido));
            else
                edtRecebido.setText(getStringInteiro($recebido));
            edtTroco.setText(getStringDouble($troco));
        }
    }

    private String getStringDouble(double valor){
        NumberFormat formatoNumerico = NumberFormat.getNumberInstance(regionalizacao);
        formatoNumerico.setMaximumFractionDigits(2);
        formatoNumerico.setMinimumFractionDigits(2);
        return formatoNumerico.format(valor);
    }
    private String getStringInteiro(double valor){
        int aux = (int) valor;
        NumberFormat formatoNumerico = NumberFormat.getNumberInstance(regionalizacao);
        return formatoNumerico.format(aux);
    }
    private final void fazRecebimento(String tipo, double pagamentoExato) {
        //inverte o sinal para o valor informado diminuir da conta
        pagamentoExato = -pagamentoExato;
        //Pega a PK do cliente
        String pkCliente = getIntent().getStringExtra("keyPK");
        //Pega a PK da conta aberta(FIADO)
        long pkFiado = MainActivity.gerenciadorBD.getPKTabela("Fiado", "FKCliente = ? AND Status = 'A'", new String[]{"" + pkCliente});
        //detalhes ItensConta:
        //1. Captura o ultimo sequencial caso ja tenha conta aberta, senao 1
        int i = MainActivity.gerenciadorBD.getMaxSqFiadoAberto(pkFiado);
        i++;
        ContentValues parColunaValor = new ContentValues();
        parColunaValor.put("Sq", i);
        parColunaValor.put("FKFiado", pkFiado);
        parColunaValor.put("Descricao", tipo);
        parColunaValor.put("Quantidade", 1);
        parColunaValor.put("PrecoUnit", pagamentoExato);
        parColunaValor.put("PrecoTotal", pagamentoExato);
        //Insere o pagamento no banco de dados
        MainActivity.gerenciadorBD.insereRegitro("ItensFiado", parColunaValor);
        //A ser usado para o envio SMS ao cliente
        //String chaveConta = pkCliente+"-"+getIntent().getStringExtra("keyIdTelefonico")+"-"+pkFiado;
        //List<String> telefones = MainActivity.gerenciadorBD.listaTelefonesContato(new Long(getIntent().getStringExtra("keyIdTelefonico")));
        //String telefone = telefones.size() > 0 ? telefones.get(0) : "";
        //Log.i("TAG", "Telefone: "+telefone);
        //verifica se a conta fechou, ou seja, se o cara pagou tudo que deve
        // OU fechou a conta por motivo de pino
        String msg;
        if(($conta+pagamentoExato==0) ? true: false) {
            String status = tipo.equals("Pino") ? "P" : "Q";
            parColunaValor = new ContentValues();
            parColunaValor.put("Status", status);
            MainActivity.gerenciadorBD.atualizarRegistros("Fiado", parColunaValor, "PK = " + pkFiado, null);
            if(status.equals("P")) {
                MainActivity.gerenciadorBD.atualizarRegistros("Cliente", parColunaValor, "PK = " + pkCliente, null);
                //ENVIAR SMS AQUI INFORMANDO O CLIENTE DO PINO
                msg = "Sua conta está aberta a muito tempo sem receber nenhum pagamento !";
            }else{
                //ENVIAR SMS AQUI INFORMANDO O CLIENTE DO PAGAMENTO
                //chaveConta = PKCliente+IdTelefone+PKFiado
                //msg = "Sua conta foi quitada em "+ Utilitaria.dataAtual(null)+".";
            }
        }else{
            //Se a conta nao for quitada, informa o cliente do pagamento parcial
            //msg = "Foi efetuado um pagamento parcial na sua com em "+ Utilitaria.dataAtual(null)+", no" +
            //        " valor de R$ "+edtRecebido.getText();
        }
        //EnviarSMS.confirmacaoEnvioSMS(this, telefone, chaveConta, msg);
        finish();
    }

    private double getDouble(String valor) {
        NumberFormat formatoNumerico = NumberFormat.getNumberInstance(regionalizacao);
        formatoNumerico.setMaximumFractionDigits(2);
        formatoNumerico.setMinimumFractionDigits(2);
        Number numero = null;
        try {
            numero = formatoNumerico.parse(valor);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numero.doubleValue();
    }
}

