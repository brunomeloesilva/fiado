package bcms.com.br.fiadoapp.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.getStringNumberInstance;
import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.getStringNumberInstance$;

/**
 * Created by brunosilva on 04/11/15.
 * CGBD = Classe Gerenciadora do Banco de Dados
 */
public final class CGBD {

    private static CGBD instanciaUnicaCGBD;
    private static Context contexto;
    private static MeuSQLiteOpenHelper meuSQLiteOpenHelper;
    private static SQLiteDatabase meuSQLiteDatabase;


    private CGBD() {}

    //Design Pattern Singleton
    public static synchronized CGBD getInstancia(Context c) {
        if (instanciaUnicaCGBD == null) {
            instanciaUnicaCGBD = new CGBD();
            contexto = c;
            //Cria o banco de dados da primeira vez que é chamado
            abrirConexao();
            fecharConexao();
        }
        return instanciaUnicaCGBD;
    }

    // Meus metodos utilitarios para o banco de dados
    private static void abrirConexao() {
        meuSQLiteOpenHelper = new MeuSQLiteOpenHelper(contexto);
        //abre o banco de dados no modo de escrita, pra poder alterar
        meuSQLiteDatabase = meuSQLiteOpenHelper.getWritableDatabase();
    }

    private static void fecharConexao(){
        if(meuSQLiteOpenHelper != null) meuSQLiteOpenHelper.close();
        if(meuSQLiteDatabase != null) meuSQLiteDatabase.close();
    }

    //As chaves primarias de todas as minhas tabelas possuem sempre o nome PK e sao sempre do tipo int
    public int getPKTabela(String tabela, String where, String[] whereArgs) {
        abrirConexao();
        Cursor cursor =  meuSQLiteDatabase.query(true, tabela, new String[]{"PK"}, where, whereArgs, null, null, null, null);
        int pk = -1;
        if(cursor.moveToFirst())
            pk = cursor.getInt(0);
        fecharConexao();
        return pk;
    }

    public long atualizarRegistros(String tabela, ContentValues parColunaValor, String where, String[] whereArgs){
        abrirConexao();
        long qtdRegistrosAlterados = meuSQLiteDatabase.update(tabela, parColunaValor, where, whereArgs);
        fecharConexao();
        return qtdRegistrosAlterados;
    }

    public long insereRegitro(String tabela, ContentValues parColunaValor){
        abrirConexao();
        long PKRegistro =  meuSQLiteDatabase.insert(tabela, "", parColunaValor);
        fecharConexao();
        return PKRegistro;
    }

    public int getMaxSqFiadoAberto(long PKFiado) {
        abrirConexao();
        String SQL =   "Select MAX(ItF.Sq) as UltimoSq "
                     + "From Cliente C Inner Join Fiado F on C.PK=F.FKCliente "
                                    + "Inner Join ItensFiado ItF on ItF.FKFiado=F.PK "
                     + "Where F.Status='A' And  ItF.FKFiado="+PKFiado;
        Cursor cursor =  meuSQLiteDatabase.rawQuery(SQL, null);
        int max = 0;
        while (cursor.moveToNext()) {
            max = cursor.getInt(0);
        }
        fecharConexao();
        return max;
    }

    public List<Map<String, String>> getListMapContasPai() {
        abrirConexao();
        final String SQL = "Select C.PK, C.idTelefone, C.Nome, SUM(ItF.PrecoTotal) "
                         + "From Cliente C Inner Join Fiado F on C.PK=F.FKCliente "
                         + "Inner Join ItensFiado ItF on ItF.FKFiado=F.PK "
                         + "Where F.Status='A' "
                         + "Group by C.PK, C.idTelefone, C.Nome "
                         + "Order by C.Nome Asc";

        Cursor cursor =  meuSQLiteDatabase.rawQuery(SQL, null);
        List<Map<String, String>> listMaps = new ArrayList<>();
        Map<String, String> map = null;
        //a regionalizacao nao deveria ser fixa !
        //Trocar este qui por um dos metodos da classe utilitaria
        NumberFormat formatoNumerico = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
        formatoNumerico.setMaximumFractionDigits(2);
        formatoNumerico.setMinimumFractionDigits(2);
        while (cursor.moveToNext()) {
            map = new HashMap<>();
            map.put("keyPK", cursor.getString(0));
            map.put("keyIdTelefonico", cursor.getString(1));
            map.put("keyNome", cursor.getString(2));
            map.put("keyTotalConta", formatoNumerico.format(cursor.getFloat(3)));
            listMaps.add(map);
        }
        fecharConexao();
        return listMaps;
    }

    //Irá trabalhar em conjunto com o metodo getListMapContasPai acima, pois este retornara
    //os filhos de cada pai
    public List<Map<String, String>> getListMapContasFilhos(String PKCliente) {
        abrirConexao();
        final String SQL = "Select ItF.Descricao, ItF.Quantidade, ItF.PrecoUnit, ItF.PrecoTotal, ItF.Data "
                         + "From Cliente C Inner Join Fiado F on C.PK=F.FKCliente "
                         + "Inner Join ItensFiado ItF on ItF.FKFiado=F.PK "
                         + "Where F.Status='A' And C.PK = " + PKCliente
                         + " ORDER BY ItF.Data Asc";
        Cursor cursor =  meuSQLiteDatabase.rawQuery(SQL, null);
        List<Map<String, String>> listMaps = new ArrayList<>();
        Map<String, String> map;
        while (cursor.moveToNext()) {
            map = new HashMap<>();
            map.put("keyDescricao", cursor.getString(0));
            map.put("keyQuantidade", getStringNumberInstance(cursor.getFloat(1), null));
            map.put("keyPrecoUnit",  getStringNumberInstance$(cursor.getFloat(2), null));
            map.put("keyPrecoTotal", getStringNumberInstance$(cursor.getFloat(3), null));
            map.put("keyData", cursor.getString(4));
            listMaps.add(map);
        }
        fecharConexao();
        return listMaps;
    }

    //Retorna o status do cliente, pra saber se pode fazer fiado pra ele
    public String getStatusCliente(String IdTelefone) {
        abrirConexao();
        Cursor cursor =  meuSQLiteDatabase.query(true, "Cliente", new String[]{"Status"}, "IdTelefone="+IdTelefone, null, null, null, null, null);
        String status = "";
        if(cursor.moveToFirst())
            status = cursor.getString(0);
        fecharConexao();
        return status;
    }

    //Aqui foi a copia dos metodos getListMapContasPai, getListMapContasFilhos com alteracoes
    //Para tela de historico - pai
    public List<Map<String, String>> getListMapContasHistoricoPai() {
        abrirConexao();
        final String SQL = "Select C.PK, C.idTelefone, C.Nome, SUM(ItF.PrecoTotal) "
                + "From Cliente C Inner Join Fiado F on C.PK=F.FKCliente "
                + "Inner Join ItensFiado ItF on ItF.FKFiado=F.PK "
                + "Where F.Status<>'A' "
                + "Group by C.PK, C.idTelefone, C.Nome "
                + "Order by C.Nome Asc";

        Cursor cursor =  meuSQLiteDatabase.rawQuery(SQL, null);
        List<Map<String, String>> listMaps = new ArrayList<>();
        Map<String, String> map = null;
        //a regionalizacao nao deveria ser fixa !
        //Trocar este qui por um dos metodos da classe utilitaria
        NumberFormat formatoNumerico = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
        formatoNumerico.setMaximumFractionDigits(2);
        formatoNumerico.setMinimumFractionDigits(2);
        while (cursor.moveToNext()) {
            map = new HashMap<>();
            map.put("keyPK", cursor.getString(0));
            map.put("keyIdTelefonico", cursor.getString(1));
            map.put("keyNome", cursor.getString(2));
            map.put("keyTotalConta", formatoNumerico.format(cursor.getFloat(3)));
            listMaps.add(map);
        }
        fecharConexao();
        return listMaps;
    }
    //Para tela de historico - filho
    public List<Map<String, String>> getListMapContasHistoricoFilhos(String PKCliente) {
        abrirConexao();
        final String SQL = "Select ItF.Descricao, ItF.Quantidade, ItF.PrecoUnit, ItF.PrecoTotal, ItF.Data, ItF.FKFiado "
                + "From Cliente C Inner Join Fiado F on C.PK=F.FKCliente "
                + "Inner Join ItensFiado ItF on ItF.FKFiado=F.PK "
                + "Where F.Status<>'A' And C.PK = " + PKCliente
                + " ORDER BY ItF.FKFiado, ItF.Data Asc";
        Cursor cursor =  meuSQLiteDatabase.rawQuery(SQL, null);
        List<Map<String, String>> listMaps = new ArrayList<>();
        Map<String, String> map;
        while (cursor.moveToNext()) {
            map = new HashMap<>();
            map.put("keyDescricao", cursor.getString(0));
            map.put("keyQuantidade", getStringNumberInstance(cursor.getFloat(1), null));
            map.put("keyPrecoUnit",  getStringNumberInstance$(cursor.getFloat(2), null));
            map.put("keyPrecoTotal", getStringNumberInstance$(cursor.getFloat(3), null));
            map.put("keyData", cursor.getString(4));
            map.put("keyFKFiado", cursor.getString(5));
            listMaps.add(map);
        }
        fecharConexao();
        return listMaps;
    }

    //Retorna o valor do pino
    public float getValorPino(String IdTelefone) {
        int PKCliente = getPKTabela("Cliente", "IdTelefone=" + IdTelefone, null);
        int PKFiado = getPKTabela("Fiado", "Status='P' And FKCliente=" + PKCliente, null);
        abrirConexao();
        Cursor cursor =  meuSQLiteDatabase.query(true, "ItensFiado", new String[]{"PrecoTotal"}, "Descricao = \"Pino\" And FKFiado=" + PKFiado, null, null, null, null, null);
       float valorFiado = 0f;
        if(cursor.moveToFirst())
            valorFiado = cursor.getFloat(0);
        fecharConexao();
        return valorFiado;
    }

    //Retorna o Sq do pino
    public int getSqPino(String IdTelefone) {
        int PKCliente = getPKTabela("Cliente", "IdTelefone=" + IdTelefone, null);
        int PKFiado = getPKTabela("Fiado", "Status='P' And FKCliente=" + PKCliente, null);
        abrirConexao();
        Cursor cursor =  meuSQLiteDatabase.query(true, "ItensFiado", new String[]{"Sq"}, "Descricao = \"Pino\" And FKFiado=" + PKFiado, null, null, null, null, null);
        int sq = 0;
        if(cursor.moveToFirst())
            sq = cursor.getInt(0);
        fecharConexao();
        return sq;
    }

    //Inicialmente usado para deletar o item fiado, mas pode ser usado para outros fins
    public long deletaRegistros(String tabela, String where, String[] whereArgs) {
        abrirConexao();
        long qtd = meuSQLiteDatabase.delete(tabela, where, whereArgs);
        fecharConexao();
        return  qtd;
    }

    //Deletar os registros de historico
    public void deleteHistorico() {
        String[] script = new String[]{
                //Deleta os ItensMovimentacao de historico
                "DELETE FROM ItensFiado WHERE FKFiado in (SELECT PK FROM Fiado WHERE Status<>'A')"
                //Deleta a Movimentacao de historico
                ,"DELETE FROM Fiado WHERE Status<>'A'"
                //Deleta os Clientes que nao possuem mas nenhum tipo de movimentacao
                ,"DELETE FROM Cliente WHERE PK NOT IN (SELECT FKCliente FROM Fiado)"
        };
        abrirConexao();
        for (String sql: script) {
            meuSQLiteDatabase.execSQL(sql);
        }
        fecharConexao();
    }

    //Retorna o numero telefonico de um contato especifico pelo IdTelefone
    public static List<String> listaTelefonesContato(long idTelefone) {
        List<String> fones = new ArrayList<>();
        Cursor cursor = contexto.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"="+idTelefone,
                null, null);
        try{
            while (cursor.moveToNext()){
                int coluna = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String fone = cursor.getString(coluna);
                fones.add(fone);
            }
        }finally {
            cursor.close();
        }
        return fones;
    }

    //Para usar este precisa da permissao WRITE_EXTERNAL_STORAGE (NÃO quer dizer que vai escrever
    // no cartão externo) para poder criar um pasta no HD (interno) do celular e um arquivo texto nela.
    public static String exportaDados() {
        abrirConexao();
        final String SQL =
                "Select '*Cliente|Cliente_Status|Fiado|Fiado_Status|Produto|Quantidade|Preco|TotalItem|Data' as registro " +
                "Union " +
                "Select " +
                "C.Nome " +
                "||'|'|| C.Status " +
                "||'|'|| F.PK " +
                "||'|'|| F.Status " +
                "||'|'|| ItF.Descricao " +
                "||'|'|| ItF.Quantidade " +
                "||'|'|| ItF.PrecoUnit " +
                "||'|'|| ItF.PrecoTotal " +
                "||'|'|| date(ItF.Data) as registro " +
                "From Cliente C Inner Join Fiado F on C.PK=F.FKCliente " +
                "               Inner Join ItensFiado ItF on ItF.FKFiado=F.PK " +
                "Order by 1";
        Cursor cursor =  meuSQLiteDatabase.rawQuery(SQL, null);
        //Nova maneira
        PrintWriter out = null;
        File file = null;
        try {
            file = getSdCardFile("FiadoApp", "BaseDados.txt");
            FileOutputStream arquivo = new FileOutputStream(file);
            out = new PrintWriter(arquivo);
            while (cursor.moveToNext())
                out.write(cursor.getString(0) + "\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            fecharConexao();
            if(out != null) out.close();
        }
        return file !=null ? file.getPath().toString() : "";
    }

    public static File getSdCardFile(String nomeDiretorio, String nomeArquivo) {
        File caminhoSdCard = android.os.Environment.getExternalStorageDirectory();
        File diretorio = new File(caminhoSdCard, nomeDiretorio);
        if(!diretorio.exists()) {
            diretorio.mkdir();
        }
        return new File(diretorio, nomeArquivo);
    }

    //Verifica se a base de dados possui registros
    public static boolean baseDadosTemRegistros() {
        abrirConexao();
        final String SQL = "Select Count(C.PK) From Cliente C Inner Join Fiado F on C.PK=F.FKCliente "
                                                    + "Inner Join ItensFiado ItF on ItF.FKFiado=F.PK ";

        Cursor cursor =  meuSQLiteDatabase.rawQuery(SQL, null);
        long qtd = 0;
        while (cursor.moveToNext()) {
            qtd = cursor.getLong(0);
        }
        fecharConexao();
        return qtd > 0 ? true : false;
    }

}
