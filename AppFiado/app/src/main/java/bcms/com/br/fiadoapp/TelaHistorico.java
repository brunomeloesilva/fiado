package bcms.com.br.fiadoapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.SparseArray;
import android.widget.ExpandableListView;

import java.util.List;
import java.util.Map;

import bcms.com.br.fiadoapp.conta.MeuBaseExpandableListAdapter;
import bcms.com.br.fiadoapp.conta.PaiFilhos;

/**
 * Created by brunosilva on 07/11/15.
 */
public class TelaHistorico extends Activity {

    //É um Map que usa primitivo na key, ao inves de Object. Mas eficiente.
    private SparseArray<PaiFilhos> map;
    private MeuBaseExpandableListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tela_historico);
        map = new SparseArray<>();
        //alimentarSparseArray();
        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        adapter = new MeuBaseExpandableListAdapter(TelaHistorico.this, map);
        expandableListView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Para atualizar a tela quando houver atualizacoes, tipo o pagamento do pino direto
        //pela tela de historico
        alimentarSparseArray();
        adapter.notifyDataSetChanged();


    }

    private void alimentarSparseArray() {
        map.clear();//deve mudar a logica para melhor desempenho
        List<Map<String, String>> listMapsContas = MainActivity.gerenciadorBD.getListMapContasHistoricoPai();

        //Caso nao haja itens no historico nao abre a tela, da aviso e fecha !
        if(listMapsContas.size()<=0) {
            //aviso de confirmacao
            AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(this);//quando eu pus getBaseContext() aqui, nao funcionou !
            construtorAlerta.setTitle("AVISO");
            construtorAlerta.setMessage("Não há itens no histórico a serem exibidos !");
            construtorAlerta.setCancelable(false);
            construtorAlerta.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            AlertDialog alerta = construtorAlerta.create();
            alerta.show();
        }

        //nem chega ate aqui se o finish() acima for executado !
        PaiFilhos mestreDetalhe;
        for(int i=0; i < listMapsContas.size(); i++) {
            mestreDetalhe = new PaiFilhos(listMapsContas.get(i).get("keyPK")
                    , listMapsContas.get(i).get("keyIdTelefonico")
                    , listMapsContas.get(i).get("keyNome")
                    , listMapsContas.get(i).get("keyTotalConta")
                    , MainActivity.gerenciadorBD.getListMapContasHistoricoFilhos(listMapsContas.get(i).get("keyPK")));
            map.append(i, mestreDetalhe);
        }
    }

}
