package bcms.com.br.fiadoapp.utilitarios;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.telephony.SmsManager;

/**
 * Created by brunosilva on 11/11/15.
 */
public final class EnviarSMS {

    //Para usar precisa declarar a permissao SEND_SMS no manifesto
    private static void enviarSMSDireto(Context context, String para, String msg){
        try{
            SmsManager smsManager = SmsManager.getDefault();
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(), 0);
            smsManager.sendTextMessage(para, null, msg, pendingIntent, null);
        } catch (Exception e) {
        }
    }

    public static void confirmacaoEnvioSMS(final Activity context, final String para, final String chaveConta, final String msg) {
        //aviso de confirmacao
        AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(context);//quando eu pus getBaseContext() aqui, nao funcionou !
        construtorAlerta.setTitle("CONFIRMAÇÃO DE ENVIO SMS");
        construtorAlerta.setMessage("Deseja notificar o cliente via SMS que houve movimentação na sua conta ?");
        construtorAlerta.setCancelable(false);
        construtorAlerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String Titulomensagem = "*** FIADO DA TABERNA ***\n";
                enviarSMSDireto(context, para, Titulomensagem+chaveConta+": "+msg);
                context.finish();
            }
        });
        construtorAlerta.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.finish();
            }
        });
        AlertDialog alerta = construtorAlerta.create();
        alerta.show();
    }
}
