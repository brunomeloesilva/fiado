package bcms.com.br.fiadoapp;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bcms.com.br.fiadoapp.conta.ContaFragment;
import bcms.com.br.fiadoapp.dao.CGBD;
import bcms.com.br.fiadoapp.telacontatos.ContatosAdapter;
import bcms.com.br.fiadoapp.telacontatos.ContatosFragment;

public class MainActivity extends AppCompatActivity {

    public static ContatosAdapter contatosAdapter;
    public static CGBD gerenciadorBD;
    private static SearchView searchView;
    private static MenuItem itemMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Inicializacao dos globais : Aqui eu carrego e monto todos os caches previamente.
        gerenciadorBD = CGBD.getInstancia(MainActivity.this);
        contatosAdapter = new ContatosAdapter(MainActivity.this);


        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Habilitar a setinha de voltar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MeuFragmentPagerAdapter adaptador = new MeuFragmentPagerAdapter(getSupportFragmentManager());
        adaptador.addFragmento(new ContaFragment(), "Conta");
        adaptador.addFragmento(new ContatosFragment(), "Contatos");
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adaptador);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                //Se NAO for a tab de contatos nao mostra a lupa de busca
                if (position == 0) {
                    //Duas vezes, pois um limpa e outro fecha, deve vir antes da invisibilidade do
                    //componente para poder sumir com o teclado se estiver aberto.
                    searchView.setIconified(true);
                    searchView.setIconified(true);
                    //Na aba de contas some com a lupa de busca !
                    itemMenu.setVisible(false);
                    //Se for a tab de contatos mostra a lupa de busca
                } else {
                    itemMenu.setVisible(true);
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    class MeuFragmentPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> tituloFragmentList = new ArrayList<>();

        public MeuFragmentPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public void addFragmento(Fragment fragmento, String titulo) {
            fragmentList.add(fragmento);
            tituloFragmentList.add(titulo);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tituloFragmentList.get(position);
        }
    }

    //Demais metodos padroes
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;*/
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
        // Associate searchable configuration with the SearchView
        itemMenu = menu.findItem(R.id.IdItemLupaBusca);
        searchView = (SearchView) itemMenu.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contatosAdapter.getFilter().filter(newText);
                return true;
            }
        });
        //Para deixar a lupa invisivel na aba inicial de conta
        itemMenu.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.IdItemAddContato:
                Intent i = new Intent(Intent.ACTION_INSERT);
                i.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(i, 1);
                break;
            case R.id.action_historico:
                Intent i2 = new Intent(getBaseContext(), TelaHistorico.class);
                startActivity(i2);
                return true;
            case R.id.action_ExcluirHistorico:
                if((gerenciadorBD.getListMapContasHistoricoPai()).size()<=0){
                    //aviso de confirmacao
                    AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(this);//quando eu pus getBaseContext() aqui, nao funcionou !
                    construtorAlerta.setTitle("AVISO");
                    construtorAlerta.setMessage("Não há itens no histórico a serem excluídos !");
                    construtorAlerta.setCancelable(false);
                    construtorAlerta.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });
                    AlertDialog alerta = construtorAlerta.create();
                    alerta.show();
                }else {
                    //aviso de confirmacao
                    AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(MainActivity.this);//quando eu pus getBaseContext() aqui, nao funcionou !
                    construtorAlerta.setTitle("CONFIRMAÇÃO DE EXCLUSÃO");
                    construtorAlerta.setMessage("Esta operação é irreversível, você deseja excluir TODO" +
                            " o histórico de contas fechadas, inclusive as que contém pino, com o intuito de liberar espaço ?");
                    construtorAlerta.setCancelable(false);
                    construtorAlerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            gerenciadorBD.deleteHistorico();
                        }
                    });
                    construtorAlerta.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });
                    AlertDialog alerta = construtorAlerta.create();
                    alerta.show();
                    return true;
                }
                break;
            case R.id.action_ExportarDados:
                if(gerenciadorBD.baseDadosTemRegistros()){
                    //INFORMA ONDE E COMO O USUARIO DEVE OBTER OS DADOS
                    AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(MainActivity.this);//quando eu pus getBaseContext() aqui, nao funcionou !
                    construtorAlerta.setTitle("EXPORTAÇÃO DE DADOS");
                    construtorAlerta.setMessage("Sua base de dados será exportada para um arquivo de texto, " +
                            "assim você poderá importá-lo no excel caso deseje !");
                    construtorAlerta.setCancelable(true);
                    construtorAlerta.setPositiveButton("Confirma", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String caminho = gerenciadorBD.exportaDados();
                            Toast.makeText(MainActivity.this, "Exportado para "+caminho, Toast.LENGTH_LONG).show();
                        }
                    });
                    construtorAlerta.setNegativeButton("Cancela", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });
                    AlertDialog alerta = construtorAlerta.create();
                    alerta.show();
                }else{
                    //MENSAGEM DIZENDO QUA A BASE NAO TEM REGISTROS
                    Toast.makeText(this, "Não há registros na base de dados !", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1)
            contatosAdapter.atualizarAdapter();
    }
}