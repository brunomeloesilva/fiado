package bcms.com.br.fiadoapp.telafiado;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bcms.com.br.fiadoapp.MainActivity;
import bcms.com.br.fiadoapp.R;
import bcms.com.br.fiadoapp.utilitarios.EnviarSMS;
import bcms.com.br.fiadoapp.utilitarios.Utilitaria;

import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.dataAtual;
import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.getFloatCurrencyInstance;
import static bcms.com.br.fiadoapp.utilitarios.Utilitaria.getStringCurrencyInstance;

/**
 * Created by brunosilva on 04/11/15.
 */
public class TelaFiado extends Activity {

    //Para poder ser manipulado entre os metodos
    private static EditText edtDescItem;
    private static EditText edtQtdItem;
    private static EditText edtPrcUnitItem;
    private static EditText edtPrcTotalItem;

    private static HashMap<String, String> mapFiado;
    private static List<Map<String, String>> listaMapsFiado;
    private static TelaFiadoAdapter telaFiadoItensAdapter;

    private static boolean itemEmEdicao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tela_fiado);

        //Nome da barra de titulo da janela
        final TextView txtTitulo = (TextView) findViewById(R.id.IdTextViewTitulo);
        txtTitulo.setText(getIntent().getStringExtra("keyNome"));

        //Captura os campos da tela para manipulacao
        edtDescItem = (EditText) findViewById(R.id.IdEditTextProduto);
        edtQtdItem = (EditText) findViewById(R.id.IdEditTextQuantidade);
        edtPrcUnitItem = (EditText) findViewById(R.id.IdEditTextPrecoUnitario);
        edtPrcTotalItem = (EditText) findViewById(R.id.IdEditTextPrecoTotalItem);
        final ImageView btnAddItemConta = (ImageView) findViewById(R.id.IdImageViewAddItemConta);
        final ImageView btnEditarItem = (ImageView) findViewById(R.id.IdImageViewEditar);
        final ImageView btnExcluirItem = (ImageView) findViewById(R.id.IdImageViewExcliur);
        final ImageView btnAddConta = (ImageView) findViewById(R.id.IdImageViewAddConta);
        final TextView txtTotalPedido = (TextView) findViewById(R.id.IdTextViewTotalPedido);

        //Conecta o adapter ao listView, com o listMap vazio mesmo, pois listMap será preenchido posteriormente
        listaMapsFiado = new ArrayList<>();
        telaFiadoItensAdapter = new TelaFiadoAdapter(getBaseContext(), listaMapsFiado);
        final ListView listView = (ListView) findViewById(R.id.IdListViewItensFiado);
        listView.setAdapter(telaFiadoItensAdapter);

        //Se tiver com o foco e todos os compos estiverem preenchidos
        /*edtDescItem.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && isPreenchimentoCampos()) {
                    btnAddItemConta.callOnClick();
                }
            }
        });*/

        //Recalcula campos ao digitar:
        edtPrcUnitItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                calculaTotalItem();
            }
        });
        edtQtdItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculaTotalItem();
            }
        });

        //Eventos dos botoes:
        //Este botao apenas atualiza os dados da tela e insere item na listView
        btnAddItemConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPreenchimentoCampos()) {

                        String labelTotalPedido = txtTotalPedido.getText().toString();
                        float totalPedido = labelTotalPedido.length() == 0 ? 0f : getFloatCurrencyInstance(labelTotalPedido, null);
                        Float totalItem = new Float(calculaTotalItem());//já faz a atualizacao dos campos e inicializa a variavel
                        float total = totalPedido + totalItem.floatValue();

                        txtTotalPedido.setText(getStringCurrencyInstance(total, null));
                        //cria e alimenta um Map com os valores dos campos da tela e insere no list do adapter
                        mapFiado = new HashMap<>();
                        mapFiado.put("keyDescricao", edtDescItem.getText().toString().trim());
                        mapFiado.put("keyQuantidade", edtQtdItem.getText().toString().trim());//getStringNumberInstance(new Float(edtQtdItem.getText().toString().trim()), null));
                        mapFiado.put("keyPrecoUnitario", edtPrcUnitItem.getText().toString().trim());
                        mapFiado.put("keyPrecoTotalItem", "" + totalItem.floatValue());
                        listaMapsFiado.add(mapFiado);

                        //limpa os campos da tela para a proxima insercao
                        edtDescItem.setText("");
                        edtQtdItem.setText("");
                        edtPrcUnitItem.setText("");
                        edtPrcTotalItem.setText("");
                        //Se nao tiver com o foco, terá !
                        //O IF se faz necessário pois faco a acao de foco em outro ponto.
                        if (!edtDescItem.hasFocus()) edtDescItem.requestFocus();
                        //Ao inserir um novo item, a lista é atualizada, desmarcando tudo.
                        telaFiadoItensAdapter.getPosicoesExclusao().clear();
                    //voltar a cor do botao normal, indicando que pode haver nova edicao.
                    if(itemEmEdicao) {
                        btnEditarItem.setImageResource(R.drawable.edit);
                        itemEmEdicao = false;
                    }
                        //Informa o adapter que o list vinculado a ele sofreu uma alteracao e ele deve se atualizar
                        telaFiadoItensAdapter.notifyDataSetChanged();
                } else
                    Toast.makeText(getBaseContext(), "Devem ser preenchidos todos os campos !", Toast.LENGTH_LONG).show();
            }
        });
        //Editar um item inserido na listagem
        btnEditarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qtdItensSelecionados = telaFiadoItensAdapter.getPosicoesExclusao().size();
                if (!itemEmEdicao && qtdItensSelecionados == 1) {
                    int posicaoDoItemNaLista = telaFiadoItensAdapter.getPosicoesExclusao().first();
                    Map<String, String> item = (Map<String, String>) telaFiadoItensAdapter.getItem(posicaoDoItemNaLista);
                    edtDescItem.setText(item.get("keyDescricao"));
                    edtQtdItem.setText(item.get("keyQuantidade"));
                    edtPrcUnitItem.setText(item.get("keyPrecoUnitario"));
                    edtPrcTotalItem.setText(item.get("keyPrecoTotalItem"));
                    btnExcluirItem.callOnClick();
                    btnEditarItem.setImageResource(R.drawable.edit_desabilitado);
                    itemEmEdicao = true;
                }else {
                    if(itemEmEdicao)
                        Toast.makeText(getBaseContext(), "Já tem um item sendo editado.", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(getBaseContext(), "Selecione UM item para editar.", Toast.LENGTH_LONG).show();

                }
            }
        });
        //para poder excluir algum item da lista, antes de fechar o pedido
        btnExcluirItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //verifico se ha algum item selecionado
                if (telaFiadoItensAdapter.getPosicoesExclusao().size() > 0) {
                    int i;
                    //orderno de forma DESC pra poder excluir
                    for (Integer pos : telaFiadoItensAdapter.getPosicoesExclusao().descendingSet()) {
                        i = pos;
                        listaMapsFiado.remove(i);
                    }
                    telaFiadoItensAdapter.notifyDataSetChanged();
                    //porque senao, na proxima vez, essas posicoes de selecoes antigas estarao aqui !
                    telaFiadoItensAdapter.limpaPosicoesExclusao();

                    //Para atualizar o valor total no IdTextViewTotalPedido
                    float totalPedido = 0;
                    for (Map<String, String> e : listaMapsFiado) {
                        totalPedido += new Float(e.get("keyPrecoTotalItem"));
                    }
                    txtTotalPedido.setText(getStringCurrencyInstance(totalPedido, null));
                } else
                    Toast.makeText(getBaseContext(), "Não há itens selecionado para exclusão !", Toast.LENGTH_LONG).show();
            }
        });
        //Este é o botao que adiciona todos os itens da lista na conta do cliente e fecha a tela
        btnAddConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listaMapsFiado.size() > 0) {
                    //master cliente
                    //Fiz pelo IdTelefonico, porque os contatos vem do banco de dados do telefone pela tela de contatos
                    String keyIdTel = getIntent().getStringExtra("keyIdTelefonico");
                    String nome = txtTitulo.getText().toString().trim();
                    ContentValues parColunaValor = new ContentValues();
                    parColunaValor.put("Nome", nome);
                    parColunaValor.put("IdTelefone", keyIdTel);
                    //Verifica se o cliente já esta cadastrado na tabela de Cliente do app
                    //caso nao esteja, cadastra.
                    long pkCliente = MainActivity.gerenciadorBD.getPKTabela("Cliente", "Nome = ? AND IdTelefone = ?", new String[]{nome, keyIdTel});
                    if (pkCliente < 0)
                        pkCliente = MainActivity.gerenciadorBD.insereRegitro("Cliente", parColunaValor);

                    //Tabela Master Fiado:
                    //1. Verificar se o cliente já possui conta aberta
                    long pkFiado = MainActivity.gerenciadorBD.getPKTabela("Fiado", "FKCliente = ? AND Status = 'A'", new String[]{""+pkCliente});
                    //2. Se possuir insere os novos itens na conta que já existe aberta
                    //3. Senao criará uma nova conta
                    if (pkFiado < 0) {
                        parColunaValor = new ContentValues();
                        parColunaValor.put("FKCliente", pkCliente);
                        pkFiado = MainActivity.gerenciadorBD.insereRegitro("Fiado", parColunaValor);
                    }
                    //detalhes ItensConta:
                    //1. Captura o ultimo sequencial caso ja tenha conta aberta, senao 1
                    int i = MainActivity.gerenciadorBD.getMaxSqFiadoAberto(pkFiado);
                    i++;
                    StringBuilder msg = new StringBuilder();
                    for (Map<String, String> e : listaMapsFiado) {
                        parColunaValor = new ContentValues();
                        parColunaValor.put("Sq", i);
                        parColunaValor.put("FKFiado", pkFiado);
                        parColunaValor.put("Descricao", e.get("keyDescricao"));
                        parColunaValor.put("Quantidade", e.get("keyQuantidade"));//getFloatNumberInstance(e.get("keyQuantidade"), null));
                        parColunaValor.put("PrecoUnit", e.get("keyPrecoUnitario"));
                        parColunaValor.put("PrecoTotal", e.get("keyPrecoTotalItem"));
                        MainActivity.gerenciadorBD.insereRegitro("ItensFiado", parColunaValor);
                        i++;
                        //msg.append("\n"+e.get("keyDescricao")+" "+e.get("keyQuantidade")+" x "+e.get("keyPrecoUnitario")+
                        //" = "+e.get("keyPrecoTotalItem"));
                    }
                    //String chaveConta = pkCliente+"-"+getIntent().getStringExtra("keyIdTelefonico")+"-"+pkFiado;
                    //List<String> telefones = MainActivity.gerenciadorBD.listaTelefonesContato(new Long(getIntent().getStringExtra("keyIdTelefonico")));
                    //String telefone = telefones.size() > 0 ? telefones.get(0) : "";
                    //EnviarSMS.confirmacaoEnvioSMS(TelaFiado.this, telefone, chaveConta, msg.toString());
                    finish();
                } else Toast.makeText(getBaseContext(), "Não há itens a serem inseridos !\nSomente itens da lista são salvos.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private float calculaTotalItem() {
        String qtdItem = edtQtdItem.getText().toString();
        String prcUnitItem = edtPrcUnitItem.getText().toString();
        qtdItem = qtdItem.length() == 0 ? "0" : qtdItem;
        prcUnitItem = prcUnitItem.length() == 0 ? "0" : prcUnitItem;

        //caso digite um ponto de inicio: Faz ajuste !
        if(prcUnitItem.length()==1 && prcUnitItem.equals(".")) {
            prcUnitItem = "0.";
        }
        if(qtdItem.length()==1 && qtdItem.equals(".")) {
            qtdItem = "0.";
        }

        Float valorTotalItem = new Float(qtdItem) * new Float(prcUnitItem);
        //Arrendonda para 2 casas decimais.
        BigDecimal arrendondado = Utilitaria.roundDouble(valorTotalItem.floatValue());
        edtPrcTotalItem.setText(arrendondado.toString());
        return arrendondado.floatValue();
    }

    private boolean isPreenchimentoCampos(){
        return edtQtdItem.getText().toString().length() != 0 &&
                edtPrcUnitItem.getText().toString().length() != 0 &&
                edtDescItem.getText().toString().length() != 0;
    }
}
