# Leia-me #
(PROJETO 1)

Este foi um projeto a titulo de estudo, apenas para me divertir e exercitar !

### O que é ? ###

* Aplicativo que visa registrar e gerenciar as vendas a titulo de fiado.
* Versão 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### O que foi usado no desenvolvimento? ###

* Android Studio 1.5.1
* SDK Mínima = API: 15 Android 4.0.3 (IceCreamSandwich)

### Algumas telas do aplicativo ###

![03.jpg](https://bitbucket.org/repo/XEEang/images/3238896439-03.jpg)
![04.jpg](https://bitbucket.org/repo/XEEang/images/223214909-04.jpg)
![06.jpg](https://bitbucket.org/repo/XEEang/images/2683668242-06.jpg)
![07.jpg](https://bitbucket.org/repo/XEEang/images/4159854971-07.jpg)
![08.jpg](https://bitbucket.org/repo/XEEang/images/1297438433-08.jpg)
![09.jpg](https://bitbucket.org/repo/XEEang/images/4212136848-09.jpg)

* O projeto tem mais telas, mais acho que você já entendeu ! :)

### Quer brincar com esse projeto? ###

* Ele é publico, basta você baixar e se divertir, aproveita pra melhorar ele ! :)
* Caso tenha alguma sugestão de melhoria ou queira relatar algum bug, informe na opção de incidências do projeto.
* Caso queira entrar em contato comigo para perguntar ou solicitar algo, me envie um email: [brunomeloesilva@gmail.com](Link URL)